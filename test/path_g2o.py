import random
import numpy as np
import math as mth
from numpy import matrix
from numpy import linalg
from math import *
import matplotlib.pyplot as plt

poses = np.load('poses.npy')

f1 = open(path1+file_name, "r")
list1 = np.genfromtxt(f1, usecols=(0,1,2), unpack = True, delimiter=" ") 

vertex_id = [['VERTEX_SE2', i] for i in range(0,len(poses))]

new_pos = np.c_[vertex_id, poses]

constraints = np.load('constraints.npy')
edge_set = [['EDGE_SE2'] for i in range(0,len(constraints))]
noise_set = [[500, 0, 0, 500, 0, 5000 ] for i in range(0,len(constraints))]
mth.floor(.0001);
col_0 = constraints[:,0]

col_1 = np.floor(constraints[:,1])
col_len = len(constraints[0])
new_cons = np.c_[edge_set,col_0.astype(int), col_1.astype(int), constraints[:,2:col_len],noise_set]

#g2o_arr = np.concatenate(new_pos,new_cons)
pos = np.array(new_pos)
#rst = np.column_stack((new_pos.flatten(),new_cons.flatten()))
fo = open("create_path.g2o", "wb")
np.savetxt(fo,new_pos , delimiter=" ",fmt="%s") 


np.savetxt(fo,new_cons,delimiter=" ",fmt="%s")
fo.close()


#print poses

#execute g2o command
import subprocess
p = subprocess.Popen(["g2o", "-o", "output.g2o","create_path.g2o"], stdout=subprocess.PIPE)
output, err = p.communicate()
print "*** Running g2o command ***\n", output

p = subprocess.Popen(["python", "plot.py", "output.g2o"], stdout=subprocess.PIPE)
output, err = p.communicate()
print "*** Running plot.py ***\n", output
