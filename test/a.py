import signal

def F():
	while True:
		pass

def signal_handler(signum, frame):
    raise Exception("Timed out!")

signal.signal(signal.SIGALRM, signal_handler)
signal.alarm(3)   # Ten seconds
try:
    F()
except Exception, msg:
    print "Timed out!"
print('hello --')