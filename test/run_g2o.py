import subprocess
import numpy as np
import matplotlib.pyplot as plt
in_map = 'map.g2o'
out_map = 'out_map.g2o'
#p = subprocess.Popen(["g2o", "-o", "output.g2o","map.g2o"], stdout=subprocess.PIPE)
#p = subprocess.Popen(["g2o","-o", "output.g2o","-typeslib","/home/siam/build_packages/vertigo/trunk/lib/libvertigo-g2o.so", "/home/siam/build_packages/vertigo/trunk/datasets/new.g2o"],stdout=subprocess.PIPE)
p = subprocess.Popen(["g2o","-o", out_map,"-typeslib","/home/siam/build_package/vertigo/trunk/lib/libvertigo-g2o.so", in_map],stdout=subprocess.PIPE)

output, err = p.communicate()
#print "*** Running g2o command ***\n", output

# p = subprocess.Popen(["python", "plot.py", "output.g2o"], stdout=subprocess.PIPE)
# output, err = p.communicate()
#print "*** Running plot.py ***\n", output
f=open(out_map)

lines=f.readlines()

x = []
y = []
x1 = []
y1 = []
c = 99999
for i in range(len(lines)):
	line=lines[i].split(' ')
	if (line[0] == 'VERTEX_SE2' or line[0] == 'VERTEX'):
		if int(line[1]) < c:
			x.append(float(line[2]))
			y.append(float(line[3]))
		else:
			x1.append(float(line[2]))
			y1.append(float(line[3]))
			
x_n = x/np.linalg.norm(x)
y_n = y/np.linalg.norm(y)

#plt.show()
plt.plot(x, y, 'y')
plt.show()
