import random
import numpy as np
import math as mth
from copy import deepcopy
from numpy import matrix
from numpy import linalg
from math import *
from array import array
import matplotlib.pyplot as plt
from write_for_g2o import *

def draw_similarity_matrix(list1,list2,ax2):
	global image_skip
	f1 = open('../comparison_graph/matlab/UofA day_evening ANN sparse matrix modifiedfindmatches alderley dataset changed parameter/'+
		'similarity_matrix.txt', "r")
	rows = len(list2)
	cols = len(list1)
	max_value = 4598//3
	m2 = 0
	all_lines = f1.read().splitlines()
	
	for m2,each_line in enumerate(all_lines[:rows:image_skip]):
		line_values = each_line.split(',')
		line_values = [int(i) for i in line_values[:cols:image_skip]]
		for m1, value in enumerate(line_values):
			if value < max_value:
				a = m1*image_skip
				b = m2*image_skip
				node_links, = ax2.plot([list1[a][0], list2[b][0]], [list1[a][1], list2[b][1]], 'r')
		





def get_pose_constraint( prev_pose,cur_pose):
	dxx = cur_pose[0] - prev_pose[0]
	dyy = cur_pose[1] - prev_pose[1]
	dt = cur_pose[2] - prev_pose[2]
	r = sqrt(dxx*dxx + dyy*dyy)
	dx = r*cos(dt)
	dy = r*sin(dt)
	return [dx, dy, dt]
#vertices = np.load('vertices.npy')
#cap4 is our first dataset
image_skip = 6
path1 = '/home/siam/Dataset-UACampus/0620.txt'
path2 = '/home/siam/Dataset-UACampus/0620modified.txt'
# path1 = '/home/siam/Dataset-UACampus/1005.txt'
# path2 = '/home/siam/Dataset-UACampus/0620.txt'
in_map = 'map.g2o'
out_map = 'output.g2o'
file_name = ''

file_name = '/wheel_odom.txt'
file_name = ''

#file_name = '/gps_odom.txt'

f1 = open(path1+file_name, "r")
list1 = np.genfromtxt(f1, usecols=(0,1,2), unpack = True, delimiter=" ") 
list1 = list1.transpose()
list1 = list1[80:350]





#----------------------------------------------------------------------------
f2 = open(path2+file_name, "r")
list2 = np.genfromtxt(f2, usecols=(0,1,2), unpack = True, delimiter=" ") 
list2 = list2.transpose()
list2 = list2[80:350]

#print np.shape(list1)
#print list1
#transform list1
a = 2*np.pi*.4/4
cos_a = cos(a)
sin_a = sin(a)

#max_y = max(list1[:,1])
# list1[:,1] = max_y - list1[:,1]
# list1[:,2] = - list1[:,2]
scale = 1
tx = -5
ty = 40
T = np.array([\
	[scale*cos_a, -sin_a, 0.0, tx],\
	[sin_a, scale*cos_a, 0.0, ty],\
	[0, 0.0, scale*1.0, 0.0],\
	[0, 0.0, 0.0, 1.0]
	])
# T = np.array([\
# 	[1, 0, 0.0, 0],\
# 	[0, 1, 0.0, 0],\
# 	[0, 0.0, 1.0, 0.0],\
# 	[0, 0.0, 0.0, 1.0]
# 	])
ones = np.ones([len(list2),1])

new_array = np.concatenate(([list2[:,0]],[list2[:,1]],[list2[:,2]],[ones[:,0]]))
new_array = new_array.transpose()
print np.shape(new_array)

print np.shape(T)
new_array = np.dot(new_array,T.transpose())
list2_before_transformation = deepcopy(list2)
list2[:,0]= new_array[:,0]
list2[:,1]= new_array[:,1]
list2[:,2]= new_array[:,2]

vertices = np.concatenate((list1, list2), axis = 0)
#vertices = list1


#pose1 = np.c_[vertices1, list1]
# loop_closures = []
# for i in range(210,270):
# 	loop_closures.append([i,len(list1)+i])
f1 = open('../comparison_graph/matlab/UofA day_evening ANN sparse matrix modifiedfindmatches alderley dataset changed parameter/matching.txt', "r")
loop_closures = np.genfromtxt(f1, usecols=(0,1), unpack = True, delimiter=",") 
loop_closures = loop_closures.transpose()
print(loop_closures)
# self_loop_closure = 10
# for i in range(0,self_loop_closure):
# 	loop_closures = np.vstack((loop_closures,[0+i,len(list1)-10+i]))
# for i in range(0,self_loop_closure):
# 	#loop_closures = np.vstack((loop_closures,[0+i,len(list1)-10+i]))
# 	loop_closures = np.vstack((loop_closures,[len(list1)+i,len(list1)+len(list2)-10+i]))
# loop_closures = np.vstack((loop_closures,[0,len(list1)-1]))
# loop_closures = np.vstack((loop_closures,[len(list1),len(list1)+len(list2)-1]))
print loop_closures[-1]
loop_closures = np.array(loop_closures)
loop_closures = loop_closures-1
loop_closures[:,1] = loop_closures[:,1]+len(list1)
#loop_closures = []
#manually create loop closure
#loop_closures = [[i,len(list1)+i] for i in range(200-80)]

#make constraint in the first loop
pose_cons1 = [np.concatenate(([i, i+1], get_pose_constraint(list1[i], list1[i+1]))) for i in range(0,len(list1)-1)]
c = len(list1)
pose_cons1 = np.array(pose_cons1)
pose_cons2 = [np.concatenate(([i+c, i+c+1], get_pose_constraint(list2[i], list2[i+1]))) for i in range(0,len(list2)-1)]
pose_cons2 = np.array(pose_cons2)


# pose_cons2[:,2] = new_array[:,0]
# pose_cons2[:,3] = new_array[:,1]
# pose_cons2[:,4] = new_array[:,2]

# pose_cons2[:,2] = -pose_cons2[:,2]
# pose_cons2[:,3] = -pose_cons2[:,3]
# pose_cons2[:,4] = -pose_cons2[:,4]


pose_cons = np.concatenate([pose_cons1, pose_cons2])
#----------------------------------------------------
#loop_closures = []
#n = 80
#c4 = 191
#c3 = 180
# for i in range(0,n):
# 	loop_closures.append([c4+i, len(list1)+c3+i])

#write_for_g2o(in_map,vertices[:], [],[],pose_cons,loop_closures)
#everticesecute g2o command
import subprocess
#p = subprocess.Popen(["g2o", "-o", "output.g2o","map.g2o"], stdout=subprocess.PIPE)
#p = subprocess.Popen(["g2o","-o", "output.g2o","-typeslib","/home/siam/build_packages/vertigo/trunk/lib/libvertigo-g2o.so", "/home/siam/build_packages/vertigo/trunk/datasets/new.g2o"],stdout=subprocess.PIPE)
p = subprocess.Popen(["g2o","-o", out_map,"-typeslib","/home/siam/build_package/vertigo/trunk/lib/libvertigo-g2o.so", in_map],stdout=subprocess.PIPE)

output, err = p.communicate()
#print "*** Running g2o command ***\n", output

# p = subprocess.Popen(["python", "plot.py", "output.g2o"], stdout=subprocess.PIPE)
# output, err = p.communicate()
#print "*** Running plot.py ***\n", output
f=open(out_map)

lines=f.readlines()


x = []
y = []
x1 = []
y1 = []
for i in range(len(lines)):
	line=lines[i].split(' ')
	if (line[0] == 'VERTEX_SE2' or line[0] == 'VERTEX'):
		if int(line[1]) < len(list1):
			x.append(float(line[2]))
			y.append(float(line[3]))
		else:
			x1.append(float(line[2]))
			y1.append(float(line[3]))
			
x_n = x/np.linalg.norm(x)
y_n = y/np.linalg.norm(y)
x_n_raw = np.concatenate([list1[:,0],list2[:,0]])
#x_n_raw = list1[:,0]
#arrow
#plt.arrow(x[0], y[0], 4, 5, head_width=5.5, head_length=.1, fc='k', ec='k')

y_n_raw = np.concatenate([list1[:,1],list2[:,1]])
#y_n_raw = list1[:,1]

x_n_raw = x_n_raw/np.linalg.norm(x_n_raw)
y_n_raw = y_n_raw/np.linalg.norm(y_n_raw)
x1_n = x1/np.linalg.norm(x1)
y1_n = y1/np.linalg.norm(y1)

#plt.show()
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)

ax1.plot(x, y, 'r')
ax1.plot(x[::image_skip],y[::image_skip],'ro',label = 'Merged Map')
ax1.plot(x1, y1, 'r')
ax1.plot(x1[::image_skip],y1[::image_skip],'ro')

#plot ground truth
#you didn't transform list2
# alight list1 to list2
tx = x[0]-list2_before_transformation[0,0]
ty = y[0]-list2_before_transformation[0,1]

x1 = [list2_before_transformation[i,0]+tx for i in range(len(list2_before_transformation))]
y1 = [list2_before_transformation[i,1]+ty for i in range(len(list2_before_transformation))]

ax1.plot(list2_before_transformation[:,0], list2_before_transformation[:,1], 'g',label='Ground Truth')
tx = x[0]-list1[0,0]
ty = y[0]-list1[0,1]

x1 = [list1[i,0]+tx for i in range(len(list1))]
y1 = [list1[i,1]+ty for i in range(len(list1))]

ax1.plot(list1[:,0], list1[:,1], 'g')
# plt.plot(list1[:,0], list1[:,1], 'g')
# plt.plot(list2[:,0], list2[:,1], 'g')


#ax2 = fig.add_subplot(212)
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
x = list1[:,0]
y = list1[:,1]
ax2.plot(x, y, 'b',linewidth=1)
ax2.plot(x[::image_skip], y[::image_skip], 'bo',label='Map 1')

# #plt.plot(list2[:,0], list2[:,1], 'g')
x2 = list2[:,0]
y2 = list2[:,1]
ax2.plot(x2, y2, 'g')
ax2.plot(x2[::image_skip], y2[::image_skip], 'go',label='Map 2')

#plt.plot(x_n[c:], y_n[c:], 'b')
#plt.plot(x_n_raw, y_n_raw, 'g')
#plt.plot(x1, y1, 'g')
#plt.plot(x_n, y_n,'y')
#show loop closures
#plt.show()

for i in range(0,len(loop_closures)-2,image_skip):
	if int(loop_closures[i][1]-len(list1)) == -112:
		continue
	a = int(loop_closures[i][0])
	b = int(loop_closures[i][1]-len(list1))
	print a,b
	#plt.plot([x[a], x1[b]], [y[a], y1[b]], 'g')
	#ax2.plot([list1[a][0], list2[b][0]], [list1[a][1], list2[b][1]], 'ro')
	node_links, = ax2.plot([list1[a][0], list2[b][0]], [list1[a][1], list2[b][1]], 'r')
	#print x[a], y[a]
#draw_similarity_matrix(list1,list2,ax2)	
#plt.show()
node_links.set_label('Node Association')
legend1 = ax1.legend(loc='upper right', shadow=True)


legend2 = ax2.legend(loc='upper right', shadow=True)
legend = legend1
frame = legend.get_frame()
frame.set_facecolor('0.90')

# Set the fontsize
for label in legend.get_texts():
    label.set_fontsize('large')

for label in legend.get_lines():
    label.set_linewidth(1.5)  # the legend line width
plt.show()



