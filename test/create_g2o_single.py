import random
import numpy as np
import math as mth
from numpy import matrix
from numpy import linalg
from math import *
from array import array
import matplotlib.pyplot as plt
from write_for_g2o import *


def get_pose_constraint( prev_pose,cur_pose):
	dxx = cur_pose[0] - prev_pose[0]
	dyy = cur_pose[1] - prev_pose[1]
	dt = cur_pose[2] - prev_pose[2]
	r = sqrt(dxx*dxx + dyy*dyy)
	dx = r*cos(dt)
	dy = r*sin(dt)
	return [dx, dy, dt]


def get_coordinate_points(pose):
	print np.shape(pose)
	points = np.zeros([len(pose),2])
	uv = np.zeros([len(pose),2])
	theta = 0
	for i in range(1, len(pose)):
		dxx = pose[i][2]
		dyy = pose[i][3]
		dt = pose[i][4]

		r = sqrt(dxx*dxx + dyy*dyy)
		theta += dt
		dx = r*cos(theta)
		dy = r*sin(theta)
		points[i,0] = points[i-1,0]+dx
		points[i,1] = points[i-1,1]+dy
		uv[i-1,0] = 3*cos(theta)
		uv[i-1,1] = 3*sin(theta)
	return [points, uv]

#vertices = np.load('vertices.npy')
#cap4 is our first dataset
path1 = '/home/siam/Dataset-UACampus/m4'
path2 = '/home/siam/Dataset-UACampus/m4'
# path1 = '/home/siam/Dataset-UACampus/1005.txt'
# path2 = '/home/siam/Dataset-UACampus/0620.txt'
in_map = 'map.g2o'
out_map = 'output.g2o'
file_name = ''

file_name = '/wheel_odom.txt'

f1 = open(path1+file_name, "r")
list1 = np.genfromtxt(f1, usecols=(0,1,2), unpack = True, delimiter=" ") 
list1 = list1.transpose()

#transform list1
a = 2*np.pi*3/4
cos_a = cos(a)
sin_a = sin(a)

max_y = max(list1[:,1])
# list1[:,1] = max_y - list1[:,1]
# list1[:,2] = - list1[:,2]
T = np.array([\
	[cos_a, -sin_a, 0.0, 4],\
	[sin_a, cos_a, 0.0, 5],\
	[0, 0.0, 1.0, 0.0],\
	[0, 0.0, 0.0, 1.0]
	])
ones = np.ones([len(list1),1])

new_array = np.concatenate(([list1[:,0]],[list1[:,1]],[list1[:,2]],[ones[:,0]]))
new_array = new_array.transpose()
print np.shape(new_array)

print np.shape(T)
new_array = np.dot(new_array,T.transpose())
# list1[:,0]= new_array[:,0]
# list1[:,1]= new_array[:,1]
# list1[:,2]= new_array[:,2]



#----------------------------------------------------------------------------
f2 = open(path2+file_name, "r")
list2 = np.genfromtxt(f2, usecols=(0,1,2), unpack = True, delimiter=" ") 
list2 = list2.transpose()

#print np.shape(list1)
#print list1

vertices = np.concatenate((list1, list2), axis = 0)
#vertices = list1


#pose1 = np.c_[vertices1, list1]
loop_closures = []
for i in range(210,270):
	loop_closures.append([i,len(list1)+i])
f1 = open('constraints.txt', "r")
loop_closures = np.genfromtxt(f1, usecols=(0,1), unpack = True, delimiter=",") 
loop_closures = loop_closures.transpose()
loop_closures = np.vstack((loop_closures, [0, len(list1)-1]))




#make constraint in the first loop
pose_cons1 = [np.concatenate(([i, i+1], get_pose_constraint(list1[i], list1[i+1]))) for i in range(0,len(list1)-1)]
c = len(list1)
pose_cons1 = np.array(pose_cons1)
pose_cons2 = [np.concatenate(([i+c, i+c+1], get_pose_constraint(list2[i], list2[i+1]))) for i in range(0,len(list2)-1)]
pose_cons2 = np.array(pose_cons2)


# pose_cons2[:,2] = new_array[:,0]
# pose_cons2[:,3] = new_array[:,1]
# pose_cons2[:,4] = new_array[:,2]

# pose_cons2[:,2] = -pose_cons2[:,2]
# pose_cons2[:,3] = -pose_cons2[:,3]
# pose_cons2[:,4] = -pose_cons2[:,4]


pose_cons = np.concatenate([pose_cons1, pose_cons2])
#----------------------------------------------------
loop_closures = []
n = 0
for i in range(0,n):
	loop_closures.append([i+300, len(list1)-n+i])

write_for_g2o(in_map,vertices[:c], pose_cons[:c-1], [] )
#everticesecute g2o command
import subprocess
#p = subprocess.Popen(["g2o", "-o", "output.g2o","map.g2o"], stdout=subprocess.PIPE)
#p = subprocess.Popen(["g2o","-o", "output.g2o","-typeslib","/home/siam/build_packages/vertigo/trunk/lib/libvertigo-g2o.so", "/home/siam/build_packages/vertigo/trunk/datasets/new.g2o"],stdout=subprocess.PIPE)
p = subprocess.Popen(["g2o","-o", out_map,"-typeslib","/home/siam/build_package/vertigo/trunk/lib/libvertigo-g2o.so", in_map],stdout=subprocess.PIPE)

output, err = p.communicate()
#print "*** Running g2o command ***\n", output

# p = subprocess.Popen(["python", "plot.py", "output.g2o"], stdout=subprocess.PIPE)
# output, err = p.communicate()
#print "*** Running plot.py ***\n", output
f=open(out_map)

lines=f.readlines()

x = []
y = []
x1 = []
y1 = []
for i in range(len(lines)):
	line=lines[i].split(' ')
	if (line[0] == 'VERTEX_SE2' or line[0] == 'VERTEX'):
		if int(line[1]) < c:
			#if np.double(line[2]) > .00001:
				x.append(np.double(line[2]))
			#else:
			#	x.append(0.0)

			#if np.double(line[3]) > .00001:
				y.append(np.double(line[3]))
			#else:
			#	y.append(0.0)
		else:
			x1.append(float(line[2]))
			y1.append(float(line[3]))
			
x_n = x/np.linalg.norm(x)
y_n = y/np.linalg.norm(y)
x_n_raw = np.concatenate([list1[:,0],list2[:,0]])
#x_n_raw = list1[:,0]
#plt.arrow(x[0], y[0], 4, 5, head_width=5.5, head_length=.1, fc='k', ec='k')

y_n_raw = np.concatenate([list1[:,1],list2[:,1]])
#y_n_raw = list1[:,1]

x_n_raw = x_n_raw/np.linalg.norm(x_n_raw)
y_n_raw = y_n_raw/np.linalg.norm(y_n_raw)
x1_n = x1/np.linalg.norm(x1)
y1_n = y1/np.linalg.norm(y1)

#plt.show()
plt.plot(x, y, 'y')
points, uv = get_coordinate_points(pose_cons1)

Q = plt.quiver(points[:,0], points[:,1], uv[:,0], uv[:,1],color='r', units='x',
               width=0.1, headwidth=1, scale=0.4, linewidths=.01, edgecolors=('k'), headaxislength=.1)
#qk = plt.quiverkey(Q, 0.5, 0.03, 1, r'$1 \frac{m}{s}$',
#                  fontproperties={'weight': 'bold'})
plt.plot(points[:,0], points[:,1], 'g')
#plt.plot(list1[:,0], list1[:,1], 'g')


#x = list1[:,0]/np.linalg.norm(list1[:,0])
#y = list1[:,1]/np.linalg.norm(list1[:,1])
#plt.plot(list2[:,0], list2[:,1], 'g')
#plt.plot(x, y, 'b')

#plt.plot(x_n[c:], y_n[c:], 'b')
#plt.plot(x_n_raw, y_n_raw, 'g')
#plt.plot(x1, y1, 'g')
#plt.plot(x_n, y_n,'y')
#show loop closures
#plt.show()
for i in range(len(loop_closures)):
	a = loop_closures[i][0]
	b = int(loop_closures[i][1])
	print a, b
	#print vertices[a], vertices[b]
	#plt.plot([x[a], x1[b]], [y[a], y1[b]], 'g')
	#plt.plot([x[a], x[b]], [y[a], y[b]], 'g')
	
#plt.show()
#print list1[:,2]
plt.show()



