import numpy as np

# noise variance will work proportionally
# If the difference among the values are very high then g2o will optimize the higher values
# it won't change the values at all which has very low variance
# So ratio of the values shouldn't be too righ
# None of the diagonal values should be zero!!
noise_pose = [1, 0, 0, .1, 0, .1]
switchable_noise_pose = [.1, 0, 0, .1, 0, .1]
noise_loop_closure = [1, 0, 0, 1, 0, 1]
#noise_loop_closure = noise_pose
v_id = 55500

def get_switchable_cons(constraints = []):
	VERTEX_SWITCH = []
	EDGE_SWITCH_PRIOR = []
	EDGE_SE2_SWITCHABLE = []
	global v_id
	switchable_cons = []
	if len(constraints) > 0:
		for i in range(v_id,v_id+len(constraints)):
			switchable_cons.append(['VERTEX_SWITCH', i, 1])
			#---------Edge switch prior-------------
			switchable_cons.append(['EDGE_SWITCH_PRIOR', i, 1, 1.0])
			
			#-----------------------------------------------
			edge_se2 = ['EDGE_SE2_SWITCHABLE']
			noise_col = switchable_noise_pose

			#pose constraintstraints
			ind = i - v_id
			x = [np.float16(constraints[ind,2])]
			y = [np.float16(constraints[ind,3])]
			theta = [np.float16(constraints[ind,4])]
			
			switchable_cons.append(np.concatenate([edge_se2, [constraints[ind,0].astype(int)],[constraints[ind,1].astype(int)], [i], x, y, theta, noise_col]))

	
 	v_id += len(constraints) 
 	switchable_cons = [''.join(str(row).strip('[]').replace('\'','').replace('\n','').replace(',','') )for row in switchable_cons] 
 	return switchable_cons







 	
def write_for_g2o(in_map, vertices = [], pose_cons = [], loop_closures = [], switchable_pose_cons = [], switchable_loop_closures = []):
	
	#vertices = np.array(vertices)
	pose_cons = np.array(pose_cons)
	loop_closures = np.array(loop_closures)
	switchable_pose_cons = np.array(switchable_pose_cons)
	switchable_loop_closures = np.array(switchable_loop_closures)
	#vertices
	vertices_id = [['VERTEX_SE2', i] for i in range(0, len(vertices))]
	vert = np.c_[vertices_id, vertices]
	#switchable loop closure
	values = [[0, 0, 0] for i in range(0, len(switchable_loop_closures))]
	switchable_loop_closures = np.c_[switchable_loop_closures, values]
	#
	#pose constraints-----------------------------------------------------
	#
	poses = []
	if len(pose_cons) > 0:
		edge_se2 = [['EDGE_SE2'] for i in range(0,len(pose_cons))]
		noise_col = [noise_pose for i in range(0,len(pose_cons))]

		#pose constraints
		x = (pose_cons[:,2]).astype(dtype= np.float16)
		y = (pose_cons[:,3]).astype(dtype= np.float16)
		theta = (pose_cons[:,4]).astype(dtype= np.float16)
		
		poses = np.c_[edge_se2, pose_cons[:,0].astype(int), pose_cons[:,1].astype(int), x, y, theta, noise_col]
	#print poses[0:]
	#
	#loop closures---------------------------------------------------------
	#
	edge_se2_1 = [['EDGE_SE2'] for i in range(0,len(loop_closures))]
	noise_col = [noise_loop_closure for i in range(0,len(loop_closures))]
	values = [[0, 0, 0] for i in range(0,len(loop_closures))]
	#print np.shape(loop_closures)
	loops = []
	if len(loop_closures) > 0:
		loops = np.c_[edge_se2_1, loop_closures[:,0].astype(int), loop_closures[:,1].astype(int), values, noise_col]

	f = open(in_map, 'w')
	np.savetxt(f, vert, delimiter=" ", fmt="%s")
	np.savetxt(f, poses, delimiter=" ", fmt="%s")
	np.savetxt(f, loops, delimiter=" ", fmt="%s")
	
	#
	#switchable pose_constraints---------------------------------------------------------
	#
	switchable_cons = get_switchable_cons(switchable_pose_cons)
	np.savetxt(f, switchable_cons, delimiter=" ", fmt="%s")
	switchable_cons = get_switchable_cons(switchable_loop_closures)
	np.savetxt(f, switchable_cons, delimiter=" ", fmt="%s")
 	

	f.close()
#vertices = np.tile([1,1, 2, 3], [6,1])
#pose_cons = np.tile([0, 1, 2, 3, 5], [6,1])
#loop_closures = []
#write_for_g2o(vertices, pose_cons)

