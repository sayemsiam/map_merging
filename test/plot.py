import matplotlib.pyplot as plt
import sys
import numpy as np

if len(sys.argv) != 2:
  print 'usage: python plot_path.py filename'
  sys.exit()
  
f=open('output.g2o')

lines=f.readlines()

x = []
y = []
x1 = []
y1 = []
for i in range(len(lines)):
  line=lines[i].split(' ')
  if (line[0] == 'VERTEX_SE2' or line[0] == 'VERTEX'):
	if int(line[1]) <= 11 :
		x.append(float(line[2]))
		y.append(float(line[3]))
	else:
		x1.append(float(line[2]))
		y1.append(float(line[3]))

x_n = x/np.linalg.norm(x)
y_n = y/np.linalg.norm(y)

x1_n = x1/np.linalg.norm(x1)
y1_n = y1/np.linalg.norm(y1)

plt.plot(x_n, y_n)
plt.plot(x1_n, y1_n,'g')
plt.show()

 
