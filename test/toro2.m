import gtsam.*

datafile = findExampleDataFile('w101.graph');
model = noiseModel.Diagonal.Sigmas([0.05; 0.05; 5* pi /180]);
[graph,initial] = gtsam.load2D(datafile, model);

%% Add a Gaussian prior on pose x_0
priorMean = Pose2(0, 0, 0);
priorNoise = noiseModel.Diagonal.Sigmas([0.01; 0.01; 0.01]);
graph.add(PriorFactorPose2(0, priorMean, priorNoise));

%% Optimize using Levenberg-Marquardt optimization and get marginals
optimizer = LevenbergMarquardtOptimizer(graph, initial);
result = optimizer.optimizeSafely;
marginals = Marginals(graph, result);
%plot3DTrajectory(result, 'r-', false); 
%axis equal;
poses = utilities.extractPose2(result);
X = poses(:,1);
Y = poses(:,2);
plot(X, Y);