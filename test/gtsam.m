%read from g2o file
clear;
import gtsam.*
%% Assumptions
%  - Robot poses are facing along the X axis (horizontal, to the right in 2D)
%  - 

%% Create the graph (defined in pose2SLAM.h, derived from NonlinearFactorGraph)
%graph initialization
graph = NonlinearFactorGraph;
fid = fopen('map.g2o');

tline = fgetl(fid);
%% Add a Gaussian prior on pose x_1
C = strsplit(tline,' ');
priorMean = Pose2(str2double(C{2}), str2double(C{3}), str2double(C{4})); % prior mean is at origin
odometryNoise = noiseModel.Diagonal.Sigmas([5.2; .2; 0.1]); % 20cm std on x,y, 0.1 rad on theta
odometryNoise = noiseModel.Diagonal.Sigmas([14.2; 6.12; 0.2]);
odometryNoise = noiseModel.Diagonal.Sigmas([20.2; 20.2; 0.1]);
priorNoise = noiseModel.Diagonal.Sigmas([1115.3; 1115.3; 0.1]); % 30cm std on x,y, 0.1 rad on theta
priorNoise
graph.add(PriorFactorPose2(0, priorMean, priorNoise)); % add directly to graph
%%graph verticess
initialEstimate = Values;

%%
i = 0;
while ischar(tline)
    %disp(tline);
    C = strsplit(tline,' ');
    i = i+1;
    if strcmp(C{1}, 'VERTEX_SE2') 
       disp(tline);
       %%add vertices in the graph
       [str2double(C{2}), str2double(C{3}), str2double(C{4}), str2double(C{5})];
       initialEstimate.insert( str2num(C{2}), Pose2(str2double(C{3}), str2double(C{4}), str2double(C{5})));
    elseif strcmp(C{1}, 'EDGE_SE2')
        disp(tline);
       %odometryNoise = noiseModel.Diagonal.Sigmas([str2double(C{7}); str2double(C{10}); str2double(C{12})]);
       graph.add(BetweenFactorPose2(str2num(C{2}), str2num(C{3}), Pose2(str2double(C{4}), str2double(C{5}), str2double(C{6})), odometryNoise));
    end
    tline = fgetl(fid);
    
end

fclose(fid);

%% Optimize using Levenberg-Marquardt optimization with an ordering from colamd
optimizer = LevenbergMarquardtOptimizer(graph, initialEstimate);
result = optimizer.optimizeSafely();
%result.print(sprintf('\nFinal result:\n  '));

%% Plot trajectory and covariance ellipses
cla;
hold on;

%plot2DTrajectory(result, [], Marginals(graph, result));

%axis([-0.6 4.8 -1 1])
axis equal
view(2)

poses = utilities.extractPose2(result);
X = poses(:,1);
Y = poses(:,2);
theta = poses(:,3);
plot(X,Y, 'g');

% keys = KeyVector(result.keys);
% xy = zeros(keys.size, 2);
% for i = 0:keys.size-1
%     key = keys.at(i);
%     pose = result.at(key);
%     xy(i+1,:) = [pose.x, pose.y];
% end
% result.size()

%plot(xy(:,1), xy(:,2), 'r');