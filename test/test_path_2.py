import random
import numpy as np
from math import *
import matplotlib.pyplot as plt
from write_for_g2o import *

out_map = 'output.g2o'
in_map = 'map.g2o'
n=3
total_nodes = 8*n
vertices=np.zeros([total_nodes, 3])
vertices_gt = np.zeros([total_nodes, 3])
var=0.02

bias = 0.001
step = 1
noise = [0, 0]
pose_cons=np.zeros([total_nodes,5])
#pose_cons[0][2:len(pose_cons[0])] = vertices[0]

# first straightline segment

#plt.plot(0, 0, 'g^', linewidth = 10)
for i in range(1, 4*n):
  if i%n == 0:
    # turn counterclockwise 90 degrees
    theta = pi/2.0
    vertices[i]=[a+b for a, b in zip(vertices[i-1], [0, 0, theta])]
    vertices_gt[i]=([a+b for a, b in zip(vertices_gt[i-1], [0, 0, theta])])
    pose_cons[i]=([i-1, i, 0, 0, theta])
    continue
  #noise = var*np.random.randn(2)
  if i%2:
    step = 2
  vl = step + noise[0]
  vr = step + noise[1]
  dx = (vl+vr)/2
  dth = vr-vl			# assume R = 0.5 
  dr = [cos(vertices[i-1][2])*dx, sin(vertices[i-1][2])*dx, dth] 
  dr_gt = [cos(vertices_gt[i-1][2])*step, sin(vertices_gt[i-1][2])*step, 0] 
  vertices[i]=([a+b for a, b in zip(vertices[i-1], dr)])
  vertices_gt[i]=([a+b for a, b in zip(vertices_gt[i-1], dr_gt)])
  pose_cons[i]=([i-1, i, dx, 0, dth])



#print pose_cons


# create loop closure pose_cons
loop_closures = []
#loop_closures.append([4*n, 8*n-1, 0, 0, 0])
#loop_closures.append([4*n-1, 0, 0, 0, 0])
for i in range(n):
  #print i, 7*n-i
  loop_closures.append([i, 6*n+i, 0, 0, 0])
#loop_closures.append([18, 10, 0, 0, 0])
#print 5*n, 'poses', n, 'loop closures'
print loop_closures
#print vertices

#np.save('poses', vertices)
#np.save('pose_cons', pose_cons)
# file1 = open('cons.tverticest', 'w')
# for item in pose_cons:
# 	file1.write('%f %f %f %f %f \n' %(item[0],item[1],item[2],item[3],item[4]))
# file1.close()

poses = np.array(vertices)
plt.arrow(poses[4*n,0], poses[4*n,1], poses[4*n+1,0] - poses[4*n,0], poses[4*n+1,1]-poses[4*n,1], head_width=0.5, head_length=.1, fc='k', ec='k')
plt.arrow(poses[0,0], poses[0,1], poses[1,0]-poses[0,0], poses[1,1]-poses[0,1], head_width=0.5, head_length=.1, fc='k', ec='k')
plt.plot(poses[:,0], poses[:,1])

poses = np.array(vertices_gt)

plt.plot(poses[:,0], poses[:,1], 'r--')
for i in range(len(loop_closures)):
  a = loop_closures[i][0]
  b = loop_closures[i][1]
  print vertices[a], vertices[b]
  plt.plot([vertices[a][0], vertices[b][0]], [vertices[a][1], vertices[b][1]], 'g')
#plt.show()

#print 'saving poses/pose_cons to poses.np/pose_cons.np as numpy arrays' 

write_for_g2o(in_map, vertices, pose_cons, loop_closures)
#everticesecute g2o command
import subprocess
#p = subprocess.Popen(["g2o", "-o", "output.g2o","map.g2o"], stdout=subprocess.PIPE)
#p = subprocess.Popen(["g2o","-o", "output.g2o","-typeslib","/home/siam/build_packages/vertigo/trunk/lib/libvertigo-g2o.so", "/home/siam/build_packages/vertigo/trunk/datasets/new.g2o"],stdout=subprocess.PIPE)
p = subprocess.Popen(["g2o","-o", out_map,"-typeslib","/home/siam/build_package/vertigo/trunk/lib/libvertigo-g2o.so", in_map],stdout=subprocess.PIPE)

output, err = p.communicate()
#print "*** Running g2o command ***\n", output

f=open(out_map)

lines=f.readlines()

x = []
y = []
x1 = []
y1 = []
for i in range(len(lines)):
	line=lines[i].split(' ')
	if (line[0] == 'VERTEX_SE2' or line[0] == 'VERTEX'):
		x.append(float(line[2]))
		y.append(float(line[3]))
		
x_n = x#/np.linalg.norm(x)
y_n = y#/np.linalg.norm(y)
#x_n_raw = np.concatenate([vertices[:,0],vertices[:,0]])
x_n_raw = vertices[:,0]

y_n_raw = np.concatenate([vertices[:,1],vertices[:,1]])
y_n_raw = vertices[:,1]
print x_n
print y_n

# x_n_raw = x_n_raw/np.linalg.norm(x_n_raw)
# y_n_raw = y_n_raw/np.linalg.norm(y_n_raw)
x1_n = x1/np.linalg.norm(x1)
y1_n = y1/np.linalg.norm(y1)

#plt.show()
plt.plot(x_n, y_n, 'y')
#plt.plot(x_n[c:], y_n[c:], 'b')
#plt.plot(x_n_raw, y_n_raw, 'g')
#plt.plot(x1, y1, 'g')
#plt.plot(x_n, y_n,'y')
#show loop closures
#plt.show()
for i in range(len(loop_closures)):
	a = loop_closures[i][0]
	b = loop_closures[i][1]
	#print vertices[a], vertices[b]
	#plt.plot([x_n_raw[a], x_n_raw[b]], [y_n_raw[a], y_n_raw[b]], 'g')
	
#plt.show()
plt.show()