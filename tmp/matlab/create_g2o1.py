import random
import numpy as np
import math as mth
from numpy import matrix
from numpy import linalg
from math import *
from array import array
import matplotlib.pyplot as plt

#poses = np.load('poses.npy')
#cap4 is our first dataset
path1 = '/home/siam/Dataset-UACampus/'
path2 = '/home/siam/Dataset-UACampus/cap3'
file_name = '/odom.txt'

f1 = open(path1+file_name, "r")
list1 = np.genfromtxt(f1, usecols=(0,1,2), unpack = True, delimiter=" ") 
list1 = list1.transpose()


#print np.shape(list1)
#print list1


vertex_id1 = [['VERTEX_SE2', i] for i in range(1,len(list1)+1)]



pose1 = np.c_[vertex_id1, list1]

#f1 = open('constraints.txt', "r")
#constraints = np.genfromtxt(f1, usecols=(0,1), unpack = True, delimiter=",") 
#constraints = constraints.transpose()
c = 0
num = 10
constraints = np.zeros([num,2])

constraints[:,0] = [i for i in range(c+1, c+num+1)]
constraints[:,1] = [i for i in range(len(list1) - num,len(list1))]
edge_set = [['EDGE_SE2'] for i in range(0,len(constraints))]
noise_set = [[0, 0, 0, 500, 0, 0, 500, 0, 5000] for i in range(0,len(constraints))]
mth.floor(.0001);
col_0 = constraints[:,0]

col_1 = np.floor(constraints[:,1])
col_len = len(constraints[0])
new_cons = np.c_[edge_set,col_0.astype(int), col_1.astype(int),noise_set]
#print new_cons

#make constraint in the first loop
constraints1 = [[i+1, i+2, list1[i+1,0]-list1[i,0],list1[i+1,1]-list1[i,1],list1[i+1,2]-list1[i,2]] for i in range(0,len(list1)-1)]

c = len(list1)
constraints1 = np.asarray(constraints1)
# thres = .0000001
# 
# for i in range(0, len(constraints1)):
# 	for j in range(0, len(constraints1[0])):
# 		if constraints1[i,j] < thres:
# 			constraints1[i,j] = 0
c1 = constraints1[:,0]
c2 = constraints1[:,1]
c1 = c1.astype(int)
c2 = c2.astype(int)
print '---------------'
print np.shape(c1)
edge_set1 = [['EDGE_SE2'] for i in range(0,len(constraints1))]
print np.shape(edge_set1)

x = (constraints1[:,2]).astype(dtype= np.float16)
y = (constraints1[:,3]).astype(dtype= np.float16)
theta = (constraints1[:,4]).astype(dtype= np.float16)
print np.shape(x)
noise1 = [[500, 0, 0, 500, 0, 5000] for i in range(0,len(constraints1))]
constraints1 = np.c_[edge_set1,c1, c2, x, y, theta, noise1]
#----------------------------------------------------
#print constraints
#print np.shape(new_cons)
#print np.shape(constraints)

#new_cons = np.concatenate(new_cons, constraints)
#g2o_arr = np.concatenate(new_pos,new_cons)

#rst = np.column_stack((new_pos.flatten(),new_cons.flatten()))
fo = open("/home/siam/map_merging/matlab/create_path.g2o", "w")
np.savetxt(fo,pose1 , delimiter=" ",fmt="%s") 


np.savetxt(fo,constraints1,delimiter=" ",fmt="%s")
np.savetxt(fo,new_cons,delimiter=" ",fmt="%s")

#np.savetxt(fo,constraints2,delimiter=" ",fmt="%s")

fo.close()



#execute g2o command
import subprocess
p = subprocess.Popen(["g2o", "-o", "/home/siam/map_merging/matlab/output.g2o","/home/siam/map_merging/matlab/create_path.g2o"], stdout=subprocess.PIPE)
output, err = p.communicate()
print "*** Running g2o command ***\n", output

p = subprocess.Popen(["python", "/home/siam/map_merging/matlab/plot.py", "/home/siam/map_merging/matlab/output.g2o"], stdout=subprocess.PIPE)
output, err = p.communicate()
print "*** Running plot.py ***\n", output



