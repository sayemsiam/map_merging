%function  d = compute_hog( path )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
hog = cv.HOGDescriptor();
%d = hog.compute.compute(imread(path));
path = '/home/siam/map_merging/trunk/datasets/nordland/64x32-grayscale-1fps/spring/images-00001.png';
im = imread(path);

h = size(im, 1);
w = size(im, 2);
scale = 2;
% im = imresize(im,scale,'bicubic','AntiAliasing',false);
reshape = [h/scale, w/scale];
[hog1, visualization] = extractHOGFeatures(im,'CellSize',[32 32]);

%im = cv.resize(im, reshape, 'Interpolation', 'Cubic');
%d = hog.compute(im, 'WinStride', [64, 32], 'Padding', [0, 0]);

%end

