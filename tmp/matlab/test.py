import matplotlib.pyplot as plt
import numpy as np
import sys

x = np.array([[1,2,3],
	[4,5,6]])
y = np.array([[1,2,3],
	[4,15,16]])
print np.shape(x)
for i in range(3):
	plt.plot(x[:,i], y[:,i], color= 'r', linewidth =2.0)
plt.show()