data=rand(100,2)*20;  % artificial data set of 100 variables (genes) and 10 samples
size(data)
data(:,2) = 5;
data(1,2) = 5;
data
[W, pc] = pca(data);
pc=pc';
W
x = ones(2);
A = mean(data);
x(1,:) = x(1,:)*A(1);
x(2,:) = x(2,:)*A(2);
plot(data(:,1),data(:,2),'.'); 
hold on;
quiver(x(1,:), x(2,:), W(1,:), W(2,:));
title('{\bf PCA} by princomp'); xlabel('PC 1'); ylabel('PC 2')

% PCA to find the sequence matching 
% [y, x, v] = find(im);
%     data = [x y];
%     [W, pc] = pca(data);
%     x = ones(2);
%     A = mean(data);
%     x(1,:) = x(1,:)*A(1);
%     x(2,:) = x(2,:)*A(2);
%     figure, plot(data(:,1),data(:,2),'.'); 
%     hold on;
%     c = 400;
%     quiver(x(1,:), x(2,:), W(1,:)*c, W(2,:)*c);
    