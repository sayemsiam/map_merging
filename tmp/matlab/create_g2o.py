import random
import numpy as np
import math as mth
from numpy import matrix
from numpy import linalg
from math import *
from array import array
import matplotlib.pyplot as plt


def get_pose_constraint( prev_pose,cur_pose):
	dxx = cur_pose[0] - prev_pose[0]
	dyy = cur_pose[1] - prev_pose[1]
	dt = cur_pose[2] - prev_pose[2]
	r = dxx*dxx + dyy*dyy
	dx = r*cos(dt)
	dy = r*sin(dt)
	return [dx, dy, dt]
#poses = np.load('poses.npy')
#cap4 is our first dataset
path1 = '/home/siam/Dataset-UACampus/4'
path2 = '/home/siam/Dataset-UACampus/3'
#file_name = '/wheel_odom.txt'

file_name = '/gps_filtered.txt'

f1 = open(path1+file_name, "r")
list1 = np.genfromtxt(f1, usecols=(0,1,2), unpack = True, delimiter=" ") 
list1 = list1.transpose()
f2 = open(path2+file_name, "r")
list2 = np.genfromtxt(f2, usecols=(0,1,2), unpack = True, delimiter=" ") 
list2 = list2.transpose()

#print np.shape(list1)
#print list1

poses = np.concatenate((list1, list2), axis = 0)
poses = list1
#print poses
#print list1[i] - list1[i-1]
print np.shape(poses)

vertex_id1 = [['VERTEX_SE2', i] for i in range(len(list1))]

vertex_id2 = [['VERTEX_SE2', i] for i in range(len(list1), len(list2)+len(list1))]
#vertex_id = np.concatenate((vertex_id1, vertex_id2), axis = 0)
vertex_id = vertex_id1

vertex_se2 = np.c_[vertex_id, poses]
pose1 = np.c_[vertex_id1, list1]

f1 = open('constraints.txt', "r")
constraints = np.genfromtxt(f1, usecols=(0,1), unpack = True, delimiter=",") 
constraints = constraints.transpose()

edge_set = [['EDGE_SE2'] for i in range(0,len(constraints))]
noise_matrix1 = [10, 0, 0,10, 0, 10]
noise_matrix = [10, 0, 0, 10, 0, 10]

noise_set = [ np.concatenate(([0, 0, 0], noise_matrix1)) for i in range(0,len(constraints))]
mth.floor(.0001);
col_0 = constraints[:,0]

col_1 = np.floor(constraints[:,1])
col_len = len(constraints[0])
matching_cons = np.c_[edge_set,col_0.astype(int), col_1.astype(int),noise_set]
#print matching_cons

#make constraint in the first loop
pose_cons1 = [np.concatenate(([i, i+1], get_pose_constraint(list1[i], list1[i+1]))) for i in range(0,len(list1)-1)]
pose_cons1 = np.asarray(pose_cons1)
# thres = .0000001
cons_theta = 4
# for i in range(0, len(pose_cons1)):
# 	if pose_cons1[i,4] < cons_theta or pose_cons1[i,4] < -cons_theta:
# 			pose_cons1[i,4] = 1
			
c = len(list1)
#print pose_cons1
print'checking...'

pose_cons2 = [np.concatenate(([i+c, i+c+1], get_pose_constraint(list2[i], list2[i+1]))) for i in range(0,len(list2)-1)]
pose_cons2 = np.asarray(pose_cons2)
#----------------------------------------------------


# for i in range(0, len(pose_cons2)):
# 		if pose_cons2[i,4] < cons_theta or pose_cons2[i,4] < -cons_theta:
# 			print 'jump theta'
# 			pose_cons2[i,4] = 1

constraints = np.concatenate((pose_cons1, pose_cons2))

c1 = pose_cons1[:,0]
c2 = pose_cons1[:,1]
c1 = c1.astype(int)
c2 = c2.astype(int)
print '---------------'
print np.shape(c1)
edge_set1 = [['EDGE_SE2'] for i in range(0,len(pose_cons1))]
print np.shape(edge_set1)

x = (pose_cons1[:,2]).astype(dtype= np.float16)
y = (pose_cons1[:,3]).astype(dtype= np.float16)
theta = (pose_cons1[:,4]).astype(dtype= np.float16)
#theta[theta>cons_theta or theta < -cons_theta] = .1

print np.shape(x)
noise1 = [noise_matrix for i in range(0,len(pose_cons1))]
pose_cons1 = np.c_[edge_set1,c1, c2, x, y, theta, noise1]


c1 = pose_cons2[:,0]
c2 = pose_cons2[:,1]
c1 = c1.astype(int)
c2 = c2.astype(int)
print '---------------'
print np.shape(c1)
edge_set2 = [['EDGE_SE2'] for i in range(0,len(pose_cons2))]
print np.shape(edge_set2)
x = (pose_cons2[:,2]).astype(dtype= np.float16)
y = (pose_cons2[:,3]).astype(dtype= np.float16)
theta = (pose_cons2[:,4]).astype(dtype= np.float16)
#theta[theta>cons_theta or theta < -cons_theta] = .1

print np.shape(x)
noise2 = [noise_matrix for i in range(0,len(pose_cons2))]
pose_cons2 = np.c_[edge_set2,c1, c2, x, y, theta, noise2]

#print constraints
#print np.shape(matching_cons)
#print np.shape(constraints)

#matching_cons = np.concatenate(matching_cons, constraints)
#g2o_arr = np.concatenate(vertex_se2,matching_cons)
pos = np.array(vertex_se2)
#rst = np.column_stack((vertex_se2.flatten(),matching_cons.flatten()))
fo = open("create_path.g2o", "w")
np.savetxt(fo,vertex_se2 , delimiter=" ",fmt="%s") 


np.savetxt(fo,pose_cons1,delimiter=" ",fmt="%s")
np.savetxt(fo,pose_cons2,delimiter=" ",fmt="%s")
np.savetxt(fo,matching_cons,delimiter=" ",fmt="%s")



fo.close()



#execute g2o command
import subprocess
p = subprocess.Popen(["g2o", "-o", "/home/siam/map_merging/matlab/output.g2o","/home/siam/map_merging/matlab/create_path.g2o"], stdout=subprocess.PIPE)
output, err = p.communicate()
print "*** Running g2o command ***\n", output

p = subprocess.Popen(["python", "/home/siam/map_merging/matlab/plot.py", "/home/siam/map_merging/matlab/output.g2o"], stdout=subprocess.PIPE)
output, err = p.communicate()
print "*** Running plot.py ***\n", output



