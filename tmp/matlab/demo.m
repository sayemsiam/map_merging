%




function demo()
    addpath(genpath('./seqSlam'));
    
%% first set the parameters

    % start with default parameters
    params = defaultParameters();    
    
    % Nordland spring dataset
    ds.name = '0620';
    total_images_loop1 = 644;
    total_images_loop2 = 672;
    
    ds.imagePath = '/home/siam/Dataset-UACampus/m2';  
    ds.fileName = '/wheel_odom.txt';
    ds.prefix='';
    ds.extension='.jpg';
    ds.suffix='';
    ds.imageSkip = 1;     % use every n-nth image
    ds.imageIndices = 1:ds.imageSkip:total_images_loop1; 
    %ds.imageIndices = repmat(ds.imageIndices,1,3);
    
    ds.savePath = 'results';
    ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));
    
    ds.preprocessing.save = 1;
    ds.preprocessing.load = 1;
    %ds.crop=[1 1 60 32];  % x0 y0 x1 y1  cropping will be done AFTER resizing!
    ds.crop=[];
    
    loop1=ds;


    % Nordland winter dataset
    ds.name = '1005';
    ds.imagePath = '/home/siam/Dataset-UACampus/m4';
    ds.imageIndices = 1:ds.imageSkip:total_images_loop2;
    %ds.imageIndices = repmat(ds.imageIndices,1,3);
    %ds.imageIndices = 400:-ds.imageSkip:300; 
    %ds.imageIndices = [ds.imageIndices 1:ds.imageSkip:300]; 
    
    ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));
    % ds.crop=[5 1 64 32];
    ds.crop=[];
    
    loop2=ds;        
    
    params.dataset(1) = loop1;
    params.dataset(2) = loop2;
    size(params.dataset(1).imageIndices)

    % load old results or re-calculate?
    params.differenceMatrix.load = 0;
    params.differenceMatrix.save = 0;
    params.contrastEnhanced.load = 0;
    params.matching.load = 0;
    
    % where to save / load the results
    params.savePath='results';
              
    
%% now process the dataset
   
    results = openSeqSLAM(params);          
    
%% show some results
    
    close all;
    % set(0, 'DefaultFigureColor', 'white');
    
    % Now results.matches(:,1) are the matched winter images for every 
    % frame from the spring dataset.
    % results.matches(:,2) are the matching score 0<= score <=1
    % The LARGER the score, the WEAKER the match.
    
    m = results.matches(:,1);
    %thresh=1;  % you can calculate a precision-recall plot by varying this threshold
    m(results.matches(:,2)>params.thresh) = NaN;  % remove the weakest matches
    %
    %building constraints as a single map
    % index of the next map will be l + ind2
    %
    %cap4 is our first dataset
    l = length(m);
    indx = find(~isnan(m));
    col2 = l+ m(indx);
    constraints = [indx, col2];
    figure, plot(m,'.');      % ideally, this would only be the diagonal
    size(m)
    %building self loop closure
    values = m(indx);
    
    im = results.DD;
    im(results.DD > 1000) = 0;
    
    figure, imshow(flipud(im));
    imwrite(im, 'im.jpg');
    save('im.mat', 'im');
    size(flipud(im));
    title('Matchings');
    match_file = 'constraints.txt'; 
    dlmwrite([match_file], constraints);
    
    merge_map(params, results);
    results.matches

end