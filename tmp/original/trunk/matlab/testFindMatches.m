 params = defaultParameters();    
params.thresh = 3*2.6/3;
%results.DD = results.DD(300:600,300:600);
params.matching.vmin = 0.0;
params.matching.vmax = 1.8;
params.matching.ds = 4; 
results.DD = [
    0 1 3 4 5 6 4 1 0 4 0 6;
    5 0 3 0 5 6 4 0 3 4 0 6;
    0 4 0 0 4 0 4 0 0 4 0 6;
    7 7 3 4 0 0 4 7 3 4 0 6;
    4 7 3 4 5 6 0 0 3 4 0 6;
    4 7 3 0 2 0 4 0 0 4 0 6;
    4 7 0 4 0 6 0 7 3 0 0 6;
    4 0 3 4 0 6 4 0 3 4 0 0;
    0 7 3 4 0 6 4 7 0 4 0 6;
    4 7 3 4 0 6 4 7 3 0 0 6;
    4 7 3 4 0 6 4 7 3 0 0 6;
    4 7 3 4 0 6 4 7 3 0 0 0;
    4 7 3 4 0 6 4 7 3 0 0 0;]


results = doFindMatches_modified1(results,params);
m = results.matches(:,1);
disp('-------------------');
size(m)
%thresh=1;  % you can calculate a precision-recall plot by varying this threshold
%m(results.matches(:,2)>params.thresh) = NaN;  % remove the weakest matches
%
%building constraints as a single map
% index of the next map will be l + ind2
%

im(results.DD > 200) = 0;

figure, imshow(flipud(im));
figure, plot(m,'.'); 
imwrite(im, 'im.jpg');
save('im.mat', 'im');
size(flipud(im));
title('Matchings');


%merge_map(params, results);
params.matching.seqMaxValue