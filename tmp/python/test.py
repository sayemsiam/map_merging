from pyflann import *
from numpy import *
from numpy.random import *
import cv2
import numpy as np
from scipy import linalg, mat, dot
from matplotlib import pyplot as plt
from sets import Set



from pyflann import *
from numpy import *
from numpy.random import *


def getHogDescriptor(path):
	img = cv2.imread(path)
	scale = 2
	h, w = img.shape[:2]
	h = h/scale 
	w = w/scale
	print np.shape(img)
	img = cv2.resize(img, (w, h), interpolation=cv2.INTER_CUBIC)
	print np.shape(img)
	hog = cv2.HOGDescriptor()
	d = hog.compute(img, winStride=(64,128), padding=(0, 0))
	return np.transpose(d)


######### hog fetures for two maps #########
value = 100
max_value = 400
path = '/home/siam/Dataset-UACampus/0620/'
d = getHogDescriptor(path+str(value)+'.pgm')
dhog1 = d;
print np.shape(d)
for i in range(1,max_value):
	value = 100 + i;
	d = getHogDescriptor(path+str(value)+'.pgm')
	dhog1 = np.vstack([dhog1, d])
	
value = 100
path = '/home/siam/Dataset-UACampus/1005/'
d = getHogDescriptor(path+str(value)+'.pgm')
dhog2 = d;
for i in range(1,max_value):
	value = 100 + i;
	d = getHogDescriptor(path+str(value)+'.pgm')
	dhog2 = np.vstack([dhog2,d])
	
dataset = dhog1
testset = dhog2
print np.shape(dhog1)
flann1 = FLANN()
#result,dists = flann.nn(dataset,testset,4,algorithm="kdtree",
#branching=32, iterations=7, checks=16);
set_distance_type("manhattan", order = 0)
params1 = flann1.build_index(dhog1, algorithm="kdtree",trees=8);
#print params1

result1, dists = flann1.nn_index(dhog2,1, checks=params1["checks"]);
S1 = set()
for i in range(0, len(result1)):
	S1 = S1|set([(i, result1[i])])
print(S1)

params2 = flann1.build_index(dhog2, algorithm="kdtree",trees=8);
#print params2
result2, dists = flann1.nn_index(dhog1,1, checks=params2["checks"]);
#print result2
S2 = set()
for i in range(0, len(result2)):
	S2 = S2|set([(result2[i], i)])
print S2
S = S1&S2
print sorted(S)

