import cv2
import numpy as np
from scipy import linalg, mat, dot
from matplotlib import pyplot as plt

dhog = [];
sum_dhog = np.zeros(shape = (1,18900))
for i in range(0,30):
	value = 100 + i;
	img = cv2.imread('0620/'+str(value)+'.jpg')
	scale = 2
	h, w = img.shape[:2]
	h = h/scale 
	w = w/scale
	img = cv2.resize(img, (w, h), interpolation=cv2.INTER_CUBIC)
	hog = cv2.HOGDescriptor()
	d = hog.compute(img, winStride=(64,128), padding=(0, 0))
	#print np.shape(d)
	dhog.append(np.transpose(d))
	sum_dhog += dhog[i] 

for i in range(0,30):
	value = 100 + i;
	img = cv2.imread('1005/'+str(value)+'.jpg')
	scale = 2
	h, w = img.shape[:2]
	h = h/scale 
	w = w/scale
	img = cv2.resize(img, (w, h), interpolation=cv2.INTER_CUBIC)
	hog = cv2.HOGDescriptor()
	d = hog.compute(img, winStride=(64,128), padding=(0, 0))
	dhog.append(np.transpose(d))
	sum_dhog += dhog[i] 
mean_dhog = sum_dhog/60
# for i in range(0,60):
# 	dhog[i] = dhog[i] - mean_dhog
similarityMatrix = np.zeros(shape=(60,60))

for i in range(0,60):
	for j in range(i,60):
		similarityMatrix[j][i] = similarityMatrix[i][j] = np.dot((dhog[i]-dhog[j]),(dhog[i]-dhog[j]).T)
print similarityMatrix
#cv2.imshow('image',similarityMatrix)
plt.imshow(similarityMatrix, cmap = 'gray')
#plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
plt.show()