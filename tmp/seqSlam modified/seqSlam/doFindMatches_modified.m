%

%      
function results = doFindMatches_modified(results, params)       
     global sfda;
    filename = sprintf('%s/matches-%s-%s%s.mat', params.savePath, params.dataset(1).saveFile, params.dataset(2).saveFile, params.saveSuffix);  
     
    if params.matching.load && exist(filename, 'file')
        display(sprintf('Loading matchings from file %s ...', filename));
        m = load(filename);
        results.matches = m.matches;          
    else
    
        matches = NaN(size(results.DD,2),2);
        
        display('Searching for matching images ...');
        % h_waitbar = waitbar(0, 'Searching for matching images.');
        
        % make sure ds is dividable by two
        params.matching.ds = params.matching.ds + mod(params.matching.ds,2);
        
        
        matches = findMatchingMatrix(results, params)
               
        % save it
        if params.matching.save
            save(filename, 'matches');
        end
        
        results.matches = matches;
        
        
    end
end

function matches = findMatchingMatrix(results, params)
    DD = (results.DD);
    % We shall search for matches using velocities between
    % params.matching.vmin and params.matching.vmax.
    % However, not every vskip may be neccessary to check. So we first find
    % out, which v leads to different trajectories:
        
    move_min = params.matching.vmin * params.matching.ds;    
    move_max = params.matching.vmax * params.matching.ds;    
    
    move = move_min:move_max;
    v = move / params.matching.ds;
    
    idy_add = repmat([0:params.matching.ds], size(v,2),1);
   % idy_add  = floor(idy_add.*v);
   
   % idy_add is y axis indices
   %   -1 0 1
   %    0 0 1
   %   -1 -1 0
   %
    idy_add = floor(idy_add .* repmat(v', 1, length(idy_add)));
    
     
    
    
    %score = zeros(2,size(DD,1));    
    
    % add a line of inf costs so that we penalize running out of data
    
    
    
    %score = zeros(1,size(DD,1));  
    %[id, vls] = min(DD);
    
    ds = params.matching.ds;
    
    DD=[DD; inf(1,size(DD,2))];
    maxRow = size(DD,1);
    matchingMatrix = NaN(size(DD));
    y_max = size(DD,1);  
    max_no = 5;
    [sortedValues,sortIndex] = sort(results.DD,'ascend'); 
    for Col = params.matching.ds/2+1 : size(results.DD,2)-params.matching.ds/2
        % this is where our trajectory starts
        n_start = Col - ds/2;
        %x  is in x axis indices, 
        x= repmat([n_start : n_start+ds], length(v), 1);  
        
        %t = Col;
        indices = sortIndex(:,Col);
        disp(indices);
        for Row= indices(1:5,:)
            %score = findSingleMatch(DD,x,idy_add,y_max, Col,Row, params);
            %Row = id;
            Row
           disp(sprintf('row-- %d ...', Row));
            xx = (x-1) * y_max;
            y = min(idy_add+Row, y_max);                
            idy = xx + y;
            score = min(sum(DD(idy),2));
            matchingMatrix(Row,Col) = score;
        end
            %   waitbar(N / size(results.DD,2), h_waitbar);
        
    end
    matchingMatrix
%     normA = matchingMatrix - min(matchingMatrix(:));
%     normA = normA ./ max(normA(:)) %
%     figure, imshow(normA);
     %matchingMatrix = normc(matchingMatrix);
%     [scores, id] = min(matchingMatrix)
%     matches = [id'+params.matching.ds/2, scores'];
matches = NaN(size(results.DD,2),2);
for Col = params.matching.ds/2+1 : size(results.DD,2)-params.matching.ds/2

    score= matchingMatrix(:,Col);
    [min_value, min_idx] = min(score);
    window = max(1, min_idx-params.matching.Rwindow/2):min(length(score), min_idx+params.matching.Rwindow/2);
    not_window = setxor(1:length(score), window);
    min_value_2nd = min(score(not_window));

    matches(Col,:) = [min_idx + params.matching.ds/2; min_value / min_value_2nd]; 
end
end
%%
function match = findSingleMatch(DD,x,idy_add,y_max, N,Row, params)


    
    %converting 2d indices to 1d indices 
    %       _ 
    %6    _
    %7 _ _ _
    %8  _
    %9 _
    %10
    
%     for s=1:size(DD,1)           
%         y = min(idy_add+s, y_max);                
%         idy = xx + y;
%         score(1,s) = min(sum(DD(idy),2));
%     end
               
        
    
    
    % find min score and 2nd smallest score outside of a window
    % around the minimum 
    
    %[min_value, min_idy] = min(score(1,:));
    %window = max(1, min_idy-params.matching.Rwindow/2):min(length(score), min_idy+params.matching.Rwindow/2);
    %not_window = setxor(1:length(score), window);
    %min_value_2nd = min(score(1,not_window));
    
    match = score;    
end
