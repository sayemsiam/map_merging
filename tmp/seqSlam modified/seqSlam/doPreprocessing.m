%


function results = doPreprocessing(params)
    for i = 1:length(params.dataset)

        % shall we just load it?
        filename = sprintf('%s/preprocessing-%s%s.mat', params.dataset(i).savePath, params.dataset(i).saveFile, params.saveSuffix);                
        if params.dataset(i).preprocessing.load && exist(filename, 'file');           
            r = load(filename);
            display(sprintf('Loading file %s ...', filename));
            results.dataset(i).preprocessing = r.results_preprocessing;
        else
            % or shall we actually calculate it?
            p = params;    
            p.dataset=params.dataset(i);
            results.dataset(i).preprocessing = single(preprocessing(p));

            if params.dataset(i).preprocessing.save                
                results_preprocessing = single(results.dataset(i).preprocessing);
                save(filename, 'results_preprocessing');
            end                
        end
    end               
end





%%
function dhog = preprocessing(params)
    

    display(sprintf('Preprocessing dataset %s, indices %d - %d ...', params.dataset.name, params.dataset.imageIndices(1), params.dataset.imageIndices(end)));
   % h_waitbar = waitbar(0,sprintf('Preprocessing dataset %s, indices %d - %d ...', params.dataset.name, params.dataset.imageIndices(1), params.dataset.imageIndices(end)));
    
    % allocate memory for all the processed images
    %     n = length(params.dataset.imageIndices);
    %     m = params.downsample.size(1)*params.downsample.size(2); 
    %     
    %     if ~isempty(params.dataset.crop)
    %         c = params.dataset.crop;
    %         m = (c(3)-c(1)+1) * (c(4)-c(2)+1);
    %     end
    %     
    %     images = zeros(m,n, 'uint8');
    %     j=1;
    dhog = [];
    % for every image ....
    '--here--'
    size(params.dataset.imageIndices)
    for i = params.dataset.imageIndices
        filename = sprintf('%s/%s%04d%s%s', params.dataset.imagePath, ...
            params.dataset.prefix, ...
            i, ...
            params.dataset.suffix, ...
            params.dataset.extension);
        
        
        filename
        % compute hog features
       
        hog = cv.HOGDescriptor();
        %d = hog.compute.compute(imread(path));
        im = imread(filename);

        h = size(im, 1);
        w = size(im, 2);
        scale = 2;
        % im = imresize(im,scale,'bicubic','AntiAliasing',false);
        reshape = [h/scale, w/scale];
        im = cv.resize(im, reshape, 'Interpolation', 'Cubic');
        d = hog.compute(im, 'WinStride', [64, 128], 'Padding', [0, 0]);
        if ~isempty(dhog)
            dhog = [dhog, d(:)];
        else
            dhog = d(:);
        end
       % waitbar((i-params.dataset.imageIndices(1)) / (params.dataset.imageIndices(end)-params.dataset.imageIndices(1)));
        
    end
    
   % close(h_waitbar);

end



