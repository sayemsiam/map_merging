params.dataset(1).fileName = '1.txt';
params.dataset(2).fileName = '2.txt';
params.dataset(2).imagePath = '';
params.dataset(1).imagePath = '';
% x = [2 4 1 2 2; 
%      4 1 0 2 2];
% y = [2 4 1 4 4; 
%      4 1 1 4 4];
% f = figure();
% hold on;
% 
% plot(x,y);
% hold off;

%first rectangle

% h = height, w = width


% ^----2--->|
% |         |
% 1         3
% |         |
% |         |
% ^<---4----|
%first rectangle
h = 10;
w = 20;
s_x = 10;
s_y = 10;
x1 = [ones(1,h),1:w, w*ones(1,h), 1:w];
y1 = [1:h, h*ones(1,w),1:h, ones(1,w)];
x1 = x1 + s_x;
y1 = y1 + s_y;
%scatter(x1,y1);
%hold on;
%second rectangle
h = 10;
w = 20;
s_x = 14;
s_y = 14;
x2 = [ones(1,h),1:w, w*ones(1,h), 1:w];
y2 = [1:h, h*ones(1,w),1:h, ones(1,w)];
x2 = x2 + s_x;
y2 = y2 + s_y;
%scatter(x2,y2, 'g');
m = ones(length(x1),2);
m(h+w+1:h+w+h, 1) = [1:h]';
m(h+w+1:h+w+h, 2) = 0;
results.matches = m;
dlmwrite(params.dataset(1).fileName, [ones(length(x1),1), x1', y1']);
dlmwrite(params.dataset(2).fileName, [ones(length(x2),1), x2', y2']);

merge_map(params, results);