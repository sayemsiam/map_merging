function  d = compute_hog( path )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
hog = cv.HOGDescriptor();
%d = hog.compute.compute(imread(path));
im = cv.imread(path);

h = size(im, 1);
w = size(im, 2);
scale = 2;
% im = imresize(im,scale,'bicubic','AntiAliasing',false);
reshape = [h/scale, w/scale];
im = cv.resize(im, reshape, 'Interpolation', 'Cubic');
d = hog.compute(im, 'WinStride', [64, 128], 'Padding', [0, 0]);

end

