function M = merge_map(params, results)
    M1 = dlmread([params.dataset(1).imagePath, params.dataset(1).fileName]);
    M2 = dlmread([params.dataset(2).imagePath, params.dataset(2).fileName]);
    M = [];
    figure;
    plot(M1(:, 1),M1(:,2) );
    hold on;
    plot(M2(:, 1),M2(:,2), 'LineWidth',3 );
    hold on;
    m = results.matches(:,1);
    %thresh=0.9;  % you can calculate a precision-recall plot by varying this threshold
    m(results.matches(:,2)>params.thresh) = NaN;  % remove the weakest matches
    
    ind_M1 = find(~isnan(m));
    ind_M2 = m(ind_M1);
    points_1 = M1(:,1:2);
    points_2 = M2(:,1:2);
    A = M1(ind_M1,1:2);
    B = M2(ind_M2,1:2);
    A = [A, ones(length(A),1)];
    a = zeros(2*length(A),4);
    
    a(1:length(A),1:3) = A(:,1:3);
    a(:,2) = -a(:,2);
    
    a(length(A)+1:end,1) = A(:,2);
    a(length(A)+1:end,2) = A(:,1);
    a(length(A)+1:end,4) = ones(length(A),1);
    
    b = zeros(2*length(B),1);
    b(1:length(B)) = B(:,1);
    b(length(B)+1:end) = B(:,2);
    lineX = [A(:,1)'; B(:,1)'];
    lineY = [A(:,2)'; B(:,2)'];
    
    %B = [B, ones(length(B),1)];
    x = a\b;
    T_x = [x(1) -x(2) x(3);
           x(2) x(1) x(4)]';
    
    p_trans = [points_1 ones(length(points_1), 1)]*T_x;
    %A_trans(:,1) = A_trans/A(:,3);
    plot(p_trans(:, 1),p_trans(:,2), 'g', 'LineWidth', 3 );
    hold on;
    plot(lineX, lineY, 'k', 'LineWidth',.5);
    
   
    T_x
   % A_trans
    size(m)
    
end