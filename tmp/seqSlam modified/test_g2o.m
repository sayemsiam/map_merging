path = '';
imageNo = 12;
indices = 1:imageNo;
extension = '.jpg'
images = zeros(length(indices),1);
for i = 1:length(indices)
    file = sprintf('%s%03d%s',path, indices(i),extension);
    im = imread(file);
    images(i) = im;
end
