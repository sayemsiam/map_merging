%



function results = openSeqSLAM(params)        
    
    results=[];
    
    % try to allocate 3 CPU cores (adjust to your machine) for parallel
    % processing
    try
        if matlabpool('size')==0
            matlabpool 3
        end
    catch
        display('No parallel computing toolbox installed.');
    end
    
    
    %
    %begin with preprocessing of the images
        if params.DO_PREPROCESSING                
            results = doPreprocessing(params);  
            size(results.dataset(1).preprocessing)
        end
        
    
    % image difference matrix             
    if params.DO_DIFF_MATRIX
        results = doDifferenceMatrix(results, params);
        size(results.D)
    end
    
    
    % contrast enhancement
    if params.DO_CONTRAST_ENHANCEMENT
        results = doContrastEnhancement(results, params);        
    else
        if params.DO_DIFF_MATRIX
            results.DD = results.D;
            %results.DD = repmat(results.D,3,2);
            
        end
    end
    
    
    % find the matches
    if params.DO_FIND_MATCHES
        
        results = doFindMatches(results, params);
        
        %size(results)
    end
            
end









