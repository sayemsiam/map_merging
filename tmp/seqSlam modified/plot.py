import matplotlib.pyplot as plt
import sys
import math

if len(sys.argv) != 2:
  print 'usage: python plot_path.py filename'
  sys.exit()
  
f=open(sys.argv[1])

lines=f.readlines()

x = []
y = []
for i in range(len(lines)):
  line=lines[i].split(' ')
  if line[0] == 'VERTEX_SE2' or line[0] == 'VERTEX':
    x.append(float(line[2]))
    y.append(float(line[3]))

plt.plot(x, y)
#plt.show()

f=open('create_path.g2o')

lines=f.readlines()

x = []
y = []
for i in range(len(lines)):
  line=lines[i].split(' ')
  if line[0] == 'VERTEX_SE2' or line[0] == 'VERTEX':
    x.append(float(line[2]))
    y.append(float(line[3]))

plt.plot(x, y, 'r')
#plt.show()

x = []
y = []
for i in range(len(lines)):
	line = lines[i].split(' ')
	if line[0] == 'EDGE_SE2' and float(line[3]) == 0.0 and float(line[4]) == 0.0:
		if abs(float(line[1]) - float(line[2])) == 1:
			print line[1], line[2]
		 
		tmp = lines[int(line[1])-1].split(' ')
		x.append(float(tmp[2]))
		y.append(float(tmp[3]))
		tmp = lines[int(line[2])-1].split(' ')
		x.append(float(tmp[2]))
		y.append(float(tmp[3]))

for i in range(0,len(x),2):
	plt.plot(x[i:i+2], y[i:i+2], 'g')
plt.show()
 
