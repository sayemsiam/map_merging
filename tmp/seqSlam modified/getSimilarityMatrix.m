

    %######### hog.compute fetures for two maps #########
    addpath(genpath('./flann'));
    value = 100
    max_value = 10
    path = '/home/siam/Dataset-UACampus/0620/';

    d = compute_hog(strcat(path, int2str(value),'.pgm'));
    'size d'
    size(d)
    dhog1 = []
    for i =1:max_value
        value = 100 + i;
        d = compute_hog(strcat(path, int2str(value), '.pgm'));
        if ~isempty(dhog1)
            dhog1 = [dhog1, d(:)];
        else
            dhog1 = d(:);
        end
    end

    value = 100;
    path = '/home/siam/Dataset-UACampus/1005/';

    size(d);
    d = compute_hog(strcat(path, int2str(value),'.pgm'));
    '--------------'
    size(d)
    dhog2 = [];
    for i =1:max_value
        value = 100 + i;
        d = compute_hog(strcat(path, int2str(value), '.pgm'));
        if ~isempty(dhog2)
            dhog2 = [dhog2, d(:)];
        else
            dhog2 = d(:);
        end
    end
    size(dhog1)
    %size(dhog)
    dataset = dhog1;
    testset = dhog2;
    size(dhog1);
    %
    %flann1 = FLANN()
    %#result,dists = flann.nn(dataset,testset,4,algorithm="kdtree",
    %#branching=32, iterations=7, checks=16);
    flann_set_distance_type('euclidean',  0);
    N = 5;
    [index1, search_params1 ] = flann_build_index(dhog1, struct('algorithm','kdtree', 'trees',8,...
                                                              'checks',64));                                             
    [result1, ndists1] = flann_search(index1, dhog2, N, search_params1);

    
    result1 = result1';
    ndists1 = ndists1';
    d1 = ndists1(:);
    tmp = (1: length(result1))';
    column1 = repmat(tmp, N, 1);
   
    
    column2 = result1(:);
    size(column2)
    S1 = table(column1, column2);
    [table2array(S1) d1]
    '------------'
   
    %% cross check
    [index2, search_params2 ] = flann_build_index(dhog2, struct('algorithm','kdtree', 'trees',8,...
                                                              'checks',64));                                             
    [result2, ndists2] = flann_search(index2, dhog1, N, search_params2);
    %result2 contains nodes of M2 for each node 1, 2, 3, .. of M1
    %result1 contains nodes of M1 for each node 1, 2, 3, .. of M2
    result2 = result2';
    ndists2 = ndists2';
    d2 = ndists2(:);
    tmp = (1: length(result2))';
    column1 = repmat(tmp, N, 1);
   
    
    column2 = result2(:);
   
    S2 = table(column1, column2);
    
    
    
    [S, i1, i2] = intersect(S1, S2);
    
    %S = (N1_i, N2_j)
    S_arr = table2array(S);
    [S_arr d1(i1)]
   
    similarity_matrix = Inf*ones(length(result2), length(result1));
    %S_arr(:,1) = nodes from map 2
    %S_arr(:,2) = nodes from map 1
    %    --------M2----------
    %    |  4 0 0 0 0 0
    %    |  1 0 0 0 0 0
    % S= M1 0 0 0 3 0 0
    %    |  0 4 0 0 0 0
    %    |  0 0 2 0 0 0
    %    |  0 1 0 0 0 0

    ndists1 = ndists1';
    ndists2 = ndists2';
    [N, M] = size(similarity_matrix);
    ind = sub2ind([N, M], S_arr(:,2), S_arr(:,1));
    
    similarity_matrix(ind) = (d1(i1) + d2(i2))/2;
    im = similarity_matrix;
    im(im > 300) = 0;
    imshow(im);
    %ind = sub2ind([N, M], S_arr(:,1), S_arr(:,2) );
    %similarity_matrix(ind) = (ndists1(S_arr(:,1)) + ndists2(S_arr(:,2)))/2;
    %similarity_matrix = (similarity_matrix + similarity_matrix');
