import numpy as np


noise_pose = [50, 0, 0, 50, 0, 50]
noise_loop_closure = [10, 0, 0, 10, 0, 10]

def write_for_g2o(in_map, vertices = [], pose_cons = [], loop_closures = []):
	
	#vertices = np.array(vertices)
	pose_cons = np.array(pose_cons)
	loop_closures = np.array(loop_closures)
	vertex_id = [['VERTEX_SE2', i] for i in range(0, len(vertices))]

	#vertices
	vert = np.c_[vertex_id, vertices]
	edge_se2 = [['EDGE_SE2'] for i in range(0,len(pose_cons))]
	noise_col = [noise_pose for i in range(0,len(pose_cons))]

	#pose constraints
	x = (pose_cons[:,2]).astype(dtype= np.float16)
	y = (pose_cons[:,3]).astype(dtype= np.float16)
	theta = (pose_cons[:,4]).astype(dtype= np.float16)
	
	poses = np.c_[edge_se2, pose_cons[:,0].astype(int), pose_cons[:,1].astype(int), x, y, theta, noise_col]
	#print poses[0:]
	edge_se2_1 = [['EDGE_SE2'] for i in range(0,len(loop_closures))]
	noise_col = [noise_loop_closure for i in range(0,len(loop_closures))]
	values = [[0, 0, 0] for i in range(0,len(loop_closures))]
	#print np.shape(loop_closures)
	loops = []
	if len(loop_closures) > 0:
		loops = np.c_[edge_se2_1, loop_closures[:,0].astype(int), loop_closures[:,1].astype(int), values, loop_closures[:,2:], noise_col]

	f = open(in_map, 'w')
	np.savetxt(f, vert, delimiter=" ", fmt="%s")
	np.savetxt(f, poses, delimiter=" ", fmt="%s")
	np.savetxt(f, loops, delimiter=" ", fmt="%s")
	f.close()

#vertices = np.tile([1,1, 2, 3], [6,1])
#pose_cons = np.tile([0, 1, 2, 3, 5], [6,1])
#loop_closures = []
#write_for_g2o(vertices, pose_cons)

