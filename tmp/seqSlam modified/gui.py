"""
This script supercedes the previous gui.py and is an improved version
wrt visual odometry computation.  The important changes are in "data2" 
function (which generates data to plot in subplot #2).  Of interest to
you are these variables:
  
* poses: poses according to (correted) wheel odometry, to be
  used for VERTEXes in g2o  

* dx, dy, dt: incremental changes in dx, dy, and heading in the local
  robot frame, to be used as local constraints EDGEs in g2o.

* pose: an intermediate list that stores the raw wheel odometry (x, y, and 
  theta) before correction

"""

import os
import sys
import glob
import re
import time
import math
import numpy as np
import scipy as sp
import cv2
import subprocess
from matplotlib import pyplot as plt
import pylab as plb
import matplotlib.animation as animation

def points2(data2):
  xs.append(data2[0][0])
  ys.append(data2[0][1])
  label_text.set_text(label_template%(data2[1]))
  line2.set_data(ys, xs)
  return line2, label_text

def updateImg(*args):
  global im1

  img1 = np.fliplr(im1.reshape(-1,3)).reshape(im1.shape)
  im.set_array(img1)
  return im,

def data2():
  global im1
  im1 = cv2.imread(imFiles[0]) 

  q = [0,0,0,1]
  pose = []          # previous pose according to wheel odometry
  poses = [[]]	     # corrected poses 
  for i in range(len(imFiles)):
    im1 = cv2.imread(imFiles[i])
    odo = open(odoFiles[i]) 
    lines = []
    for line in odo:
      lines.append(line)

    # read (x,y) of the next pose according to odometry
    x = float(re.findall("-?\d+.\d+", lines[1])[0])
    y = float(re.findall("-?\d+.\d+", lines[2])[0])

    # read orientation expressed in quaternion and compute Euler angle th_z 

    q[0] = float(re.findall("-?\d+.\d+", lines[5])[0])
    q[1] = float(re.findall("-?\d+.\d+", lines[6])[0])
    q[2] = float(re.findall("-?\d+.\d+", lines[7])[0])
    q[3] = float(re.findall("-?\d+.\d+", lines[8])[0])

    # for how this equation works, refer to 
    # en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
    # we deal with phi in the special case of q0 = q1 = 0

    th = math.atan2(2*q[2]*q[3], 1 - 2*q[2]*q[2])
  
    if i == 0:
      # initialize intermeidate and historic pose variables
      pose = [x, y, th]
      poses.append(pose)
    else:
      # compute incremental changes in x, y and theta in world frame   
      dxw = x - pose[0]
      dyw = y - pose[1]
      dt = th - pose[2]

      # update "pose" for next iteration
      pose = [x, y, th] 
     
      # make sure the angular change doesn't jump
      if dt > np.pi:
        dt -= 2*np.pi
      elif dt < -np.pi:
        dt += 2*np.pi

      # !!! this is robot-specific: our robot wheel odometry is not correct
      # 0.47 works but any number close should be fine
      dt = dt*0.5 
    
      # calculate linear change in x of local frame
      dx = math.sqrt(dxw*dxw + dyw*dyw)
      dy = 0 		

      # finally, produce the correct pose
      t = poses[-1][2] + dt 
      x = poses[-1][0] + dx*math.cos(t) 
      y = poses[-1][1] + dx*math.sin(t) 

      poses.append([x, y, t])

    # send to point2 to plot
    yield [y, x], i
  # end for-loop 

  print 'Done simulation ..' 
  time.sleep(5) 
  sys.exit(1)

dir = '/home/siam/Dataset-UACampus/0620/'
os.chdir(dir)


imFiles=[]
for file in glob.glob("*.pgm"):
  imFiles.append(file)
imFiles = sorted(imFiles, key=lambda x: int(x.split('.')[0]))
print(len(imFiles))

odoFiles=[]
for file in glob.glob("*.txt"):
    odoFiles.append(file)

odoFiles = sorted(odoFiles, key=lambda x: int(x.split('.')[0]))

fig = plt.figure(figsize=(12, 9))

ax1 = fig.add_subplot(221)
plb.setp(plb.gca(), 'xticks', [])
plb.setp(plb.gca(), 'yticks', [])

im1 = cv2.imread(imFiles[0]) 
im1 = np.fliplr(im1.reshape(-1,3)).reshape(im1.shape)
im = plt.imshow(im1)

ani1 = animation.FuncAnimation(fig, updateImg, interval=10, blit=False)

xs = []
ys = []
ax2 = fig.add_subplot(222)

line2, = ax2.plot(xs, ys)
ax2.set_ylim(-300, 100)
plb.setp(plb.gca(), 'yticks', [-200, -100, 0, 100], 
                    'yticklabels', [-200, -100, 0, 100])
ax2.set_xlim(-200, 200)
plb.setp(plb.gca(), 'xticks', [-100, -0, 100], 
                    'xticklabels', [-100, 0, 100])

label_template = '%3d steps'    # prints running number of steps
label_text = ax2.text(0.05, 0.9, '', transform=ax2.transAxes)

ani2 = animation.FuncAnimation(fig, points2, data2, blit=False,\
     interval=10, repeat=True)

ax3 = fig.add_subplot(223)
plb.setp(plb.gca(), 'xticks', [])
plb.setp(plb.gca(), 'yticks', [])

ax4 = fig.add_subplot(224)
plb.setp(plb.gca(), 'xticks', [])
plb.setp(plb.gca(), 'yticks', [])

plt.show()
