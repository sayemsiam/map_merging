



%% first set the parameters
    clear all;
    NN = 10;
    seq_length = [30,100,320];
    
    time = zeros(NN*length(seq_length),6);
    for ll=1:length(seq_length)
    for n=NN:-1:1
        % start with default parameters
        params = defaultParameters();    
        params.matching.ds = seq_length(ll);
        % Nordland spring dataset
        ds.name = 'spring';
        ds.imagePath = '../../datasets/existing dataset/qut_dataset/nordland/spring'; 
        ds.numberFormat =5;
        ds.cellSize = [16, 16];
        ds.prefix='images-';
        ds.extension='.png';
        ds.suffix='';
        ds.imageSkip = n*1;     % use every n-nth image
        ds.imageIndices = 1:ds.imageSkip:35700;    
        ds.savePath = 'results';
        ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));

        ds.preprocessing.save = 0;
        ds.preprocessing.load = 0;
        %ds.crop=[1 1 60 32];  % x0 y0 x1 y1  cropping will be done AFTER resizing!
        ds.crop=[];

        spring=ds;


        % Nordland winter dataset
        ds.name = 'winter';
        ds.imagePath = '../../datasets/existing dataset/qut_dataset/nordland/winter';       
        ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));
        % ds.crop=[5 1 64 32];
        ds.crop=[];

        winter=ds;        

        params.dataset(1) = spring;
        params.dataset(2) = winter;

        % load old results or re-calculate?
        params.differenceMatrix.load = 0;
        params.contrastEnhanced.load = 0;
        params.matching.load = 0;
        
        params.differenceMatrix.save = 0;
        params.contrastEnhanced.save = 0;
        params.matching.save = 0;

        % where to save / load the results
        params.savePath='results';


    %% now process the dataset

        results = openSeqSLAM(params);          

    %% show some results

        close all;
        % set(0, 'DefaultFigureColor', 'white');

        % Now results.matches(:,1) are the matched winter images for every 
        % frame from the spring dataset.
        % results.matches(:,2) are the matching score 0<= score <=1
        % The LARGER the score, the WEAKER the match.

        m = results.matches(:,1);
        %ploting precision recall graph
        targs = 1:length(results.matches);
        range = 4;
        filename = sprintf('prcurve_nordland_ds%s_gray%s_resize%s_Contrast%s.mat',num2str(params.matching.ds), ...
        num2str(params.DO_GRAYLEVEL), ...
        num2str(params.DO_RESIZE ), ...
        num2str(params.DO_CONTRAST_ENHANCEMENT));   
        showPrecisionCurve(results.matches,targs,range,1,filename);


        mean_value = mean(results.matches(:,2));
        im = results.D;

        figure, imagesc(im);

        mean_value
        thresh=mean_value*0;  % you can calculate a precision-recall plot by varying this threshold
        %m(results.matches(:,2)>thresh) = NaN;  % remove the weakest matches
        figure,plot(m,'.');      % ideally, this would only be the diagonal
        title('Matchings');
        ind = n+(ll-1)*10;
        time(ind,:) = [params.matching.ds, params.dataset(1).imageSkip, results.time_preprocessing, results.time_differenceMatrix, results.time_contrastEnhancement, results.time_findMatches];
        clear params;
        dlmwrite('current.txt', time);
    end
    end
   
    dlmwrite('time_original.txt', time);