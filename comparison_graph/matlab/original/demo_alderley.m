

%% first set the parameters
    clear all
    load fm;
    % start with default parameters
    params = defaultParameters();   
    
    ds.imageSkip = 4;     % use every n-nth image
    % alderley night dataset
    ds.name = 'night';
    ds.imagePath = '../../datasets/existing dataset/qut_dataset/alderley/FRAMESA';    
    ds.imageIndices = 1:ds.imageSkip:1696;%0;     
    ds.numberFormat =5;
    ds.prefix='Image';
    ds.extension='.jpg';
    ds.suffix='';
    
   
    ds.savePath = 'results';
    ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));
    
    ds.preprocessing.save = 1;
    ds.preprocessing.load = 0;
    % load old results or re-calculate?
    params.differenceMatrix.load = 0;
    params.contrastEnhanced.load = 0;
    params.matching.load = 0;
    %ds.crop=[1 1 60 32];  % x0 y0 x1 y1  cropping will be done AFTER resizing!
    ds.crop=[];
    
    spring=ds;


    % Alderley day dataset
    ds.name = 'day';
    ds.imagePath = '../../datasets/existing dataset/qut_dataset/alderley/FRAMESB'; 
     ds.imageIndices = 1:ds.imageSkip:1460;%7;    
    ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));
    % ds.crop=[5 1 64 32];
    ds.crop=[];
    
    winter=ds;        

    params.dataset(1) = spring;
    params.dataset(2) = winter;

    
    
    % where to save / load the results
    params.savePath='results';
              
    
%% now process the dataset
   
    results = openSeqSLAM(params);          
    
%% show some results
    
    close all;
    % set(0, 'DefaultFigureColor', 'white');
    
    % Now results.matches(:,1) are the matched winter images for every 
    % frame from the spring dataset.
    % results.matches(:,2) are the matching score 0<= score <=1
    % The LARGER the score, the WEAKER the match.
    results.matches
    m = results.matches(:,1);
    %ploting precision recall graph
    targs = fm;
    range = 4;
    filename = sprintf('no_contrast_prcurve_alderley_ds%s_gray%s_resize%s.mat',num2str(params.matching.ds), ...
    num2str(params.DO_GRAYLEVEL), ...
    num2str(params.DO_RESIZE ) ...
    );   
    showPrecisionCurve(results.matches,targs,range,ds.imageSkip,filename);
    
    results.matches
    mean_value = mean(results.matches(:,2));
    im = results.D;
    %im = flipud(im);
    %im= im*255/params.initial_distance;
    figure, imagesc(im);
    set(gca,'Ydir','normal');
    mean_value
    thresh=109999900;  % you can calculate a precision-recall plot by varying this threshold
    %m(results.matches(:,2)>thresh) = NaN;  % remove the weakest matches
    figure,plot(m,'.');      % ideally, this would only be the diagonal
    xlabel(params.dataset(2).name);
    ylabel(params.dataset(1).name);
    title('Matchings');               
