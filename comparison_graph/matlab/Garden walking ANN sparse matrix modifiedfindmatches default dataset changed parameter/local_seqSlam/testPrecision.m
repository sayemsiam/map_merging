a = [1,.4;
    2, .5;
    3, .0;
    NaN, .9;
    NaN, .0;
    NaN, .2;
    NaN, .3];

targs = [1:length(a)];
% Compute empirical curves

[TPR_emp, FPR_emp, PPV_emp] = precision_recall(targs, a(:,2)', a(:,1)');
TPR_emp
PPV_emp
cols = [200 45 43; 37 64 180; 0 176 80; 0 0 0]/255;
figure; hold on;
plot(TPR_emp, PPV_emp, '-o', 'color', cols(1,:), 'linewidth', 2);
axis([0 1 0 1]);
xlabel('TPR (recall)'); ylabel('PPV (precision)'); title('PR curves');
set(gca, 'box', 'on');
