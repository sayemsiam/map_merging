%% first set the parameters

    clear all;
   
    % start with default parameters
    params = defaultParameters(); 
%     params.DO_PREPROCESSING = 1;
%     params.DO_RESIZE        = 1;
%     params.DO_GRAYLEVEL     = 1;
%     params.DO_PATCHNORMALIZATION    = 1;
%     params.DO_SAVE_PREPROCESSED_IMG = 1;
%     params.DO_DIFF_MATRIX   = 1;
      params.DO_CONTRAST_ENHANCEMENT  = 0;
%     params.DO_FIND_MATCHES  = 1;


    params.downsample.size = [32 32];  % height, width
    params.matching.ds = 30;
    params.N = 30;
    params.K = 2;
    addpath(genpath('../fast_seqSlam'));
     % 
   
    ds.name = 'evening';
    ds.imageSkip = 1;     % use every n-nth image
    ds.imagePath = '../../datasets/UofA_dataset/day_evening/evening'; 
    d_size = 645;
    ds.imageIndices = 1:ds.imageSkip:d_size;     
    ds.numberFormat = 1;
    ds.prefix='';
    ds.cellSize = [8, 4];
    ds.extension='.jpg';
    ds.suffix='';
    
    
    ds.savePath = 'results';
    ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));
    
    ds.preprocessing.save = 1;
    ds.preprocessing.load = 1;
    % load old results or re-calculate?
    params.differenceMatrix.load = 0;
    params.differenceMatrix.save = 0;
    params.contrastEnhanced.load = 0;
    params.matching.load = 0;
    
    params.matching.load = 0;
    %ds.crop=[1 1 60 32];  % x0 y0 x1 y1  cropping will be done AFTER resizing!
    ds.crop=[];
    
    spring=ds;


    % 
    ds.name = 'day';
    ds.imagePath = '../../datasets/UofA_dataset/day_evening/day';   
    %ds.imageIndices = 1:ds.imageSkip:1640;     
          
    ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));
    % ds.crop=[5 1 64 32];
    ds.crop=[];
    
    winter=ds;        

    params.dataset(1) = spring;
    params.dataset(2) = winter;

    
    
    % where to save / load the results
    params.savePath='results';
              
    
%% now process the dataset
   
    results = openSeqSLAM(params);      
    
    
%% show some results
    
    close all;
    % set(0, 'DefaultFigureColor', 'white');
    
    % Now results.matches(:,1) are the matched winter images for every 
    % frame from the spring dataset.
    % results.matches(:,2) are the matching score 0<= score <=1
    % The LARGER the score, the WEAKER the match.
    
    m = results.matches(:,1);
    thresh=.95;  % you can calculate a precision-recall plot by varying this threshold
    %m(results.matches(:,2)>thresh) = NaN;  % remove the weakest matches
    %results.matches(:,1)=m;
    %ploting precision recall graph
    targs = 1:d_size;
    range = 6;
    filename = sprintf('no_hog_prcurve_U0fA_day_evening_ds%s_gray%s_resize%s_N%s.mat',num2str(params.matching.ds), ...
    num2str(params.DO_GRAYLEVEL), ...
    num2str(params.DO_RESIZE ), ...
    num2str(params.N));   
    showPrecisionCurve(results.matches,targs,range,params.dataset(1).imageSkip,filename);
    
    
    mean_value = mean(results.matches(:,2));
    im = results.DD;
    %im = flipud(im);
    %im= im*255/params.initial_distance;
    f = figure();
    imagesc(im);
    figure, imagesc(results.seqValues);
    addpath(genpath('/home/siam/map_merging/comparison_graph/matlab/graphs/altmany-export_fig-113e357'));
    print2eps('difference_matrix',f);
    set(gca,'Ydir','normal');
    mean_value
    save('results_ua','results');
    figure,plot(m,'.');      % ideally, this would only be the diagonal
    xlabel(params.dataset(2).name);
    ylabel(params.dataset(1).name);
    title('Matchings'); 
    results.timeSpent
