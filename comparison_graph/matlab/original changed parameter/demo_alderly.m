




%% first set the parameters

    % start with default parameters
    params = defaultParameters();    
    
    % Alderley day dataset
    ds.name = 'day';
    ds.imagePath = '../../datasets/existing dataset/qut_dataset/alderley/FRAMESB';    
    ds.prefix='Image';
    ds.extension='.jpg';
    ds.suffix='';
    ds.imageSkip = 50;     % use every n-nth image
    ds.imageIndices = 1:ds.imageSkip:14607;    
    ds.savePath = 'results';
    ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));
    
    ds.preprocessing.save = 1;
    ds.preprocessing.load = 1;
    %ds.crop=[1 1 60 32];  % x0 y0 x1 y1  cropping will be done AFTER resizing!
    ds.crop=[];
    
    spring=ds;


    % alderley night dataset
    ds.name = 'night';
    ds.imagePath = '../../datasets/existing dataset/qut_dataset/alderley/FRAMESA';    
    ds.imageIndices = 1:ds.imageSkip:16960;     
    ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));
    % ds.crop=[5 1 64 32];
    ds.crop=[];
    
    winter=ds;        

    params.dataset(1) = spring;
    params.dataset(2) = winter;

    % load old results or re-calculate?
    params.differenceMatrix.load = 0;
    params.contrastEnhanced.load = 0;
    params.matching.load = 0;
    
    % where to save / load the results
    params.savePath='results';
              
    
%% now process the dataset
   
    results = openSeqSLAM(params);          
    
%% show some results
    
    close all;
    % set(0, 'DefaultFigureColor', 'white');
    
    % Now results.matches(:,1) are the matched winter images for every 
    % frame from the spring dataset.
    % results.matches(:,2) are the matching score 0<= score <=1
    % The LARGER the score, the WEAKER the match.
    
    m = results.matches(:,1);
    
    thresh=0.9;  % you can calculate a precision-recall plot by varying this threshold
    %m(results.matches(:,2)>thresh) = NaN;  % remove the weakest matches
    plot(m,'.');      % ideally, this would only be the diagonal
    title('Matchings');                 
