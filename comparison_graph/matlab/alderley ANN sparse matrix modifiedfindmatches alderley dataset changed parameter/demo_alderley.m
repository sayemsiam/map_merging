% 

% Copyright 2013, Niko Sünderhauf
% niko@etit.tu-chemnitz.de
%
% This file is part of OpenSeqSLAM.
%
% OpenSeqSLAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% OpenSeqSLAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with OpenSeqSLAM.  If not, see <http://www.gnu.org/licenses/>.




%% first set the parameters
    clear all;
    load fm;
    % start with default parameters
    params = defaultParameters();    
    addpath(genpath('../fast_seqSlam'));
     % Alderley day dataset
    load fm
    ds.name = 'night';%FRAMESA night
    ds.imageSkip = 4;     % use every n-nth image
    ds.imagePath = '../../datasets/existing dataset/qut_dataset/alderley/FRAMESA'; 
    ds.imageIndices = 1:ds.imageSkip:1696;%0;     
    ds.numberFormat = 5;
    ds.prefix='Image';
    ds.cellSize = [8, 8];
    ds.extension='.jpg';
    ds.suffix='';
    
    
    ds.savePath = 'results';
    ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));
    
    ds.preprocessing.save = 1;
    ds.preprocessing.load = 1;
    %ds.crop=[1 1 60 32];  % x0 y0 x1 y1  cropping will be done AFTER resizing!
    ds.crop=[];
    
    spring=ds;


    % alderley night dataset
    ds.name = 'day';% FRAMESB day
    ds.imagePath = '../../datasets/existing dataset/qut_dataset/alderley/FRAMESB';   
    ds.imageIndices = 1:ds.imageSkip:1460;%7;     
          
    ds.saveFile = sprintf('%s-%d-%d-%d', ds.name, ds.imageIndices(1), ds.imageSkip, ds.imageIndices(end));
    % ds.crop=[5 1 64 32];
    ds.crop=[];
    
    winter=ds;        

    params.dataset(1) = spring;
    params.dataset(2) = winter;

    % load old results or re-calculate?
    params.differenceMatrix.load = 0;
    params.contrastEnhanced.load = 0;
    params.matching.load = 0;
    
    % where to save / load the results
    params.savePath='results';
              
    
%% now process the dataset
   
    results = openSeqSLAM(params);          
    
%% show some results
    
    close all;
    % set(0, 'DefaultFigureColor', 'white');
    
    % Now results.matches(:,1) are the matched winter images for every 
    % frame from the spring dataset.
    % results.matches(:,2) are the matching score 0<= score <=1
    % The LARGER the score, the WEAKER the match.
    
    m = results.matches(:,1);
    %ploting precision recall graph
    targs = fm;
    range = 4;
    filename = sprintf('prcurve_nordland_ds%s_gray%s_resize%s_N%s.mat',num2str(params.matching.ds), ...
    num2str(params.DO_GRAYLEVEL), ...
    num2str(params.DO_RESIZE ), ...
    num2str(params.N ));   
    showPrecisionCurve(results.matches,targs,range,ds.imageSkip,filename);
    thresh=.95;
    results.matches(results.matches(:,2)>thresh,1) = NaN;
    mean_value = mean(results.matches(:,2));
    im = results.D;
    %im = flipud(im);
    %im= im*255/params.initial_distance;
    figure, imagesc(im);
    set(gca,'Ydir','normal');
    mean_value
    
    m(results.matches(:,2)>thresh) = NaN;  % remove the weakest matches
    figure,plot(m,'.');      % ideally, this would only be the diagonal
    xlabel(params.dataset(2).name);
    ylabel(params.dataset(1).name);
    title('Matchings');  
    results.timeSpent
